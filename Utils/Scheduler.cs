using System;
using WeatherHelper.Services;

namespace WeatherHelper.Utils
{
    public static class Scheduler
    {
        public static void IntervalInSeconds(int hour, int min, double interval, long chatId, Action task)
        {
            interval /= 3600;
            SchedulerService.GetInstance.ScheduleTask(hour, min, interval, chatId, task);
        }

        public static void IntervalInMinutes(int hour, int min, double interval, long chatId, Action task)
        {
            interval /= 60;
            SchedulerService.GetInstance.ScheduleTask(hour, min, interval, chatId, task);
        }

        public static void IntervalInHours(int hour, int min, double interval, long chatId, Action task)
        {
            SchedulerService.GetInstance.ScheduleTask(hour, min, interval, chatId, task);
        }

        public static void IntervalInDays(int hour, int min, double interval, long chatId, Action task)
        {
            interval *= 24;
            SchedulerService.GetInstance.ScheduleTask(hour, min, interval, chatId, task);
        }

        public static void RemoveScheduleTask(long chatId)
        {
            SchedulerService.GetInstance.RemoveScheduleTask(chatId);
        }
    }
}