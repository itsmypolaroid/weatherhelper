using System.Collections.Generic;
using WeatherHelper.Models.ClothesModels;

namespace WeatherHelper.Utils
{
    public class WearBuilder
    {
        private readonly ClothesRoot _clothes = new ClothesRoot();

        public WearBuilder WithHead(HeadWear headWear)
        {
            _clothes.HeadWear = headWear;
            return this;
        }

        public WearBuilder WithTop(TopWear topWear)
        {
            _clothes.TopWear = topWear;
            return this;
        }

        public WearBuilder WithBottom(BottomWear bottomWear)
        {
            _clothes.BottomWear = bottomWear;
            return this;
        }

        public WearBuilder WithFoot(FootWear footWear)
        {
            _clothes.FootWear = footWear;
            return this;
        }

        public WearBuilder WithUnderwear(UnderWear underWear)
        {
            _clothes.UnderWear = underWear;
            return this;
        }

        public WearBuilder WithOuter(OuterWear outerWear)
        {
            _clothes.OuterWear = outerWear;
            return this;
        }

        public WearBuilder WithAccessories(List<Accessory> accessories)
        {
            _clothes.Accessories = accessories;
            return this;
        }

        public ClothesRoot Build()
        {
            return _clothes;
        }
    }
}