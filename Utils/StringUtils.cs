using System;
using System.Linq;

namespace WeatherHelper.Utils
{
    public static class StringUtils
    {
        public static string FirstCharToUpper(string input)
        {
            if (string.IsNullOrEmpty(input))
                throw new ArgumentException("empty string!");
            return input.First().ToString().ToUpper() + input.Substring(1);
        }
    }

}