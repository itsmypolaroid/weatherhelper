using System;
using System.Collections.Generic;

namespace WeatherHelper.Utils
{
    public static class Chooser
    {
        private static readonly Random random = new Random();
        public static string GetRandomString(List<string> stringList)
        {
            int randIndex = random.Next(stringList.Count);
            return stringList[randIndex];
        }

        public static T GetRandomItem<T>(T[] data)
        {
            return data[random.Next(0, data.Length)];
        }
        
        public static string GenerateRandomName(int nLength)
        {
            int length = nLength;
            char[] chars = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
            String rndString = "";

            for (int i = 0; i < length; i++)
                rndString += chars[random.Next(chars.Length)];

            return rndString;
        }
        
        public static string GenerateRandomNumber(int nLength)
        {
            char[] chars = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            int charsNo = 9;
            int length = nLength;
            String rndString = "";

            for (int i = 0; i < length; i++)
                rndString += chars[random.Next(charsNo)];

            return rndString;
        }
    }
}