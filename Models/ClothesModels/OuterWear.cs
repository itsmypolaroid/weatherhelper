using System.Collections.Generic;
using Newtonsoft.Json;

namespace WeatherHelper.Models.ClothesModels
{
    public class OuterWear
    {
        /// <summary>
        /// Куртки
        /// </summary>
        [JsonProperty("jackets")]
        public List<string> Jacket { get; set; }
        /// <summary>
        /// Пальто
        /// </summary>
        [JsonProperty("coats")]
        public List<string> Coats { get; set; }
        /// <summary>
        /// Плащ
        /// </summary>
        [JsonProperty("cloaks")]
        public List<string> Cloaks { get; set; }
    }
}