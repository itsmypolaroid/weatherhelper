using System.Collections.Generic;
using Newtonsoft.Json;

namespace WeatherHelper.Models.ClothesModels
{
    public class FootWear
    {
        /// <summary>
        /// Туфли
        /// </summary>
        [JsonProperty("shoes")]
        public List<string> Shoes { get; set; }
        /// <summary>
        /// Кроссовки
        /// </summary>
        [JsonProperty("sneakers")]
        public List<string> Sneakers { get; set; }
        /// <summary>
        /// Сандали
        /// </summary>
        [JsonProperty("sandals")]
        public List<string> Sandals { get; set; }
        /// <summary>
        /// Сапоги
        /// </summary>
        [JsonProperty("boots")]
        public List<string> Boots { get; set; } 
    }
}