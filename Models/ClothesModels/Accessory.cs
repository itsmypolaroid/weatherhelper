using System.Collections.Generic;
using Newtonsoft.Json;

namespace WeatherHelper.Models.ClothesModels
{
    public class Accessory
    {
        /// <summary>
        /// Сережки
        /// </summary>
        [JsonProperty("earrings")]
        public List<string> Earrings { get; set; }
        /// <summary>
        /// Кольца
        /// </summary>
        [JsonProperty("rings")]
        public List<string> Rings { get; set; }
        /// <summary>
        /// Цепочки на шею, ожерелья
        /// </summary>
        [JsonProperty("necklaces")]
        public List<string> Necklaces { get; set; }
        /// <summary>
        /// Браслеты
        /// </summary>
        [JsonProperty("bracelets")]
        public List<string> Bracelets { get; set; }
        /// <summary>
        /// Часы
        /// </summary>
        [JsonProperty("watches")]
        public List<string> Watches { get; set; }
    }
}