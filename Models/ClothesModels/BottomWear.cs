using System.Collections.Generic;
using Newtonsoft.Json;

namespace WeatherHelper.Models.ClothesModels
{
    public class BottomWear
    {
        /// <summary>
        /// Джинсы
        /// </summary>
        [JsonProperty("jeans")]
        public List<string> Jeans { get; set; }
        /// <summary>
        /// Юбка
        /// </summary>
        [JsonProperty("skirts")]
        public List<string> Skirts { get; set; }
        /// <summary>
        /// Шорты
        /// </summary>
        [JsonProperty("shorts")]
        public List<string> Shorts { get; set; }
        /// <summary>
        /// Брюки
        /// </summary>
        [JsonProperty("trousers")]
        public List<string> Trousers { get; set; }
    }
}