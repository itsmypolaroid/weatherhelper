using System.Collections.Generic;
using Newtonsoft.Json;

namespace WeatherHelper.Models.ClothesModels
{
    public class UnderWear
    {
        /// <summary>
        /// Колготки
        /// </summary>
        [JsonProperty("tights")]
        public List<string> Tights { get; set; }
        /// <summary>
        /// Носки
        /// </summary>
        [JsonProperty("socks")]
        public List<string> Socks { get; set; }
        /// <summary>
        /// Чулки
        /// </summary>
        [JsonProperty("stockings")]
        public List<string> Stockings { get; set; }
    }
}