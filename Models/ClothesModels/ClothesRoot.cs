using System.Collections.Generic;
using Newtonsoft.Json;

namespace WeatherHelper.Models.ClothesModels
{
    public class ClothesRoot
    {
        [JsonProperty("head")]
        public HeadWear HeadWear { get; set; } = new HeadWear();

        [JsonProperty("top")]
        public TopWear TopWear { get; set; } = new TopWear();

        [JsonProperty("bottom")]
        public BottomWear BottomWear { get; set; } = new BottomWear();

        [JsonProperty("footwear")]
        public FootWear FootWear { get; set; } = new FootWear();
        
        [JsonProperty("underwear")]
        public UnderWear UnderWear { get; set; } = new UnderWear();
        
        [JsonProperty("outerwear")]
        public OuterWear OuterWear { get; set; } = new OuterWear();

        [JsonProperty("accessories")]
        public List<Accessory> Accessories { get; set; } = new List<Accessory>();
    }
}