using System.Collections.Generic;
using Newtonsoft.Json;

namespace WeatherHelper.Models.ClothesModels
{
    public class HeadWear
    {
        /// <summary>
        /// Шляпы, Шапки
        /// </summary>
        [JsonProperty("hats")]
        public List<string> Hats { get; set; }
        /// <summary>
        /// Кепки
        /// </summary>
        [JsonProperty("caps")]
        public List<string> Caps { get; set; }
        /// <summary>
        /// Прочие головные уборы
        /// </summary>
        [JsonProperty("others")]
        public List<string> Others { get; set; }
    }
}