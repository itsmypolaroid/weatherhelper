using System.Collections.Generic;
using Newtonsoft.Json;

namespace WeatherHelper.Models.ClothesModels
{
    public class TopWear
    {
        /// <summary>
        /// Футболки, топы
        /// </summary>
        [JsonProperty("t_shirts")]
        public List<string> TShirts { get; set; }
        /// <summary>
        /// Рубашки, блузки
        /// </summary>
        [JsonProperty("shirts")]
        public List<string> Shirts { get; set; }
        /// <summary>
        /// Платья, сарафаны
        /// </summary>
        [JsonProperty("dresses")]
        public List<string> Dresses { get; set; }
        /// <summary>
        /// Свитера, джемперы, кардиганы, толстовки
        /// </summary>
        [JsonProperty("sweaters")]
        public List<string> Sweater { get; set; }
    }
}