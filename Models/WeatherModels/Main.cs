using Newtonsoft.Json;

namespace WeatherHelper.Models.WeatherModels
{
    public class Main
    {
        [JsonProperty("temp")]
        public double Temperature { get; set; }
        
        [JsonProperty("temp_min")]
        public double MinTemperature { get; set; }
        
        [JsonProperty("temp_max")]
        public double MaxTemperature { get; set; }
    }
    public enum TempFeeling
    {
        None,
        Dubak,
        Cold,
        Freeze,
        Cool,
        PreComfort,
        Comfort,
        Hot,
        Roast
    }
}