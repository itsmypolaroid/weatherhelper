using Newtonsoft.Json;

namespace WeatherHelper.Models.WeatherModels
{
    public class Wind
    {
        [JsonProperty("speed")] 
        public double Speed { get; set; }
    }
    public enum WindPower
    {
        None,
        Quiet,
        Easy,
        Medium,
        Strong,
        Powerful,
        Hurricane
    }
}