using System.Collections.Generic;
using Newtonsoft.Json;

namespace WeatherHelper.Models.WeatherModels
{
    public class WeatherRoot
    {
        [JsonProperty("weather")]
        public List<Weather> Weather { get; set; } = new List<Weather>();

        [JsonProperty("main")]
        public Main MainWeather { get; set; } = new Main();

        [JsonProperty("wind")]
        public Wind Wind { get; set; } = new Wind();

        [JsonProperty("name")]
        public string Name { get; set; } = string.Empty;

//        [JsonIgnore]
//        public string DisplayTemp => $"Температура: {Math.Round(MainWeather?.Temperature ?? 0)}°," +
//                                     $" {Weather?[0]?.Description ?? string.Empty}";
    }
}