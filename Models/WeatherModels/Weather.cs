using Newtonsoft.Json;

namespace WeatherHelper.Models.WeatherModels
{
    public class Weather
    {
        [JsonProperty("main")]
        public string Main { get; set; }
        
        [JsonProperty("description")]
        public string Description { get; set; }
    }
}