using Newtonsoft.Json;

namespace WeatherHelper.Models
{
    public class User
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("timer")]
        public string Timer { get; set; }
    }
}