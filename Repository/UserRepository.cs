using WeatherHelper.Database;
using WeatherHelper.Models;

namespace WeatherHelper.Repository
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public WeatherContext WeatherContext => Context as WeatherContext;
        public UserRepository(WeatherContext context) : base(context)
        {
            
        }
    }
}