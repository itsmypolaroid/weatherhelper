using WeatherHelper.Database;
using WeatherHelper.Models.ClothesModels;

namespace WeatherHelper.Repository
{
    public class ClotheRepository : Repository<ClothesRoot>, IClothesRepository
    {
        public WeatherContext WeatherContext => Context as WeatherContext;

        public ClotheRepository(WeatherContext context) : base(context)
        {
            
        }
    }
}