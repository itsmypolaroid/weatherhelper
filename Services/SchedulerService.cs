using System;
using System.Collections.Generic;
using System.Threading;

namespace WeatherHelper.Services
{
    public class SchedulerService
    {
        private static SchedulerService _instance;
        private readonly Dictionary<long, Timer> _timers = new Dictionary<long, Timer>();

        private SchedulerService()
        {
        }

        public static SchedulerService GetInstance => _instance ?? (_instance = new SchedulerService());

        public void ScheduleTask(int hour, int minutes, double intervalInHour, long chatId, Action task)
        {
            DateTime now = DateTime.Now;
            DateTime firstRun = new DateTime(now.Year, now.Month, now.Day, hour, minutes, 0, 0);
            if (now > firstRun) {
                firstRun = firstRun.AddDays(1);
            }

            TimeSpan timeToAction = firstRun - now;

            if (timeToAction <= TimeSpan.Zero) {
                timeToAction = TimeSpan.Zero;
            }
            
            var timer = new Timer(x =>
            {
                task.Invoke();
            }, null, timeToAction, TimeSpan.FromHours(intervalInHour));
            
            if (_timers.ContainsKey(chatId)) {
                _timers[chatId].Dispose();
                _timers.Remove(chatId);
            }
            _timers.Add(chatId, timer);
        }

        public void RemoveScheduleTask(long chatId)
        {
            if (_timers.Count <= 0) {
                return;
            }
            if (_timers.ContainsKey(chatId)) {
                _timers[chatId].Dispose();
                _timers.Remove(chatId);
            }
        }
    }
}