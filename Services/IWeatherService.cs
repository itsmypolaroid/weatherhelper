using System.Threading.Tasks;

namespace WeatherHelper.Services
{
    public interface IWeatherService
    {
        Task<string> GetWeatherAsync(string city);
    }
}