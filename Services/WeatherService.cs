using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WeatherHelper.Models.WeatherModels;
using WeatherHelper.Utils;
using static Newtonsoft.Json.JsonConvert;

namespace WeatherHelper.Services
{
    public class WeatherService : IWeatherService
    {
        private const string WEATHER_BASE_URI = "http://api.openweathermap.org/data/2.5/weather?";
        private const string CITY_URI_PART = "q={0}&units=metric&lang=ru&appid=";
        private const string API_WEATHER_KEY = "97c0eb7ae1abc8e01d3e18abca57599f";
        
        public async Task<string> GetWeatherAsync(string city)
        {
            if (string.IsNullOrEmpty(city)) {
                return await Task.FromException<string>(new ArgumentException("Вы не указали город," +
                                                                              "воспользуйтесь форматом - город:москва"));
            }

            WeatherRoot weatherRoot;
            using (var client = new HttpClient()) {
                var weatherUrl = string.Format(WEATHER_BASE_URI + CITY_URI_PART + API_WEATHER_KEY, city.ToLower());
                var json = await client.GetStringAsync(weatherUrl);

                if (string.IsNullOrWhiteSpace(json))
                    return null;

                weatherRoot = DeserializeObject<WeatherRoot>(json);
            }

            return await Task.Run(() => GetWeatherMessage(city, weatherRoot));
        }
        
        private string GetWeatherMessage(string city, WeatherRoot weatherRoot)
        {
            var currentTemp = Math.Round(weatherRoot.MainWeather.Temperature);
            var minTemp = Math.Round(weatherRoot.MainWeather.MinTemperature);
            var maxTemp = Math.Round(weatherRoot.MainWeather.MaxTemperature);
            var message = new StringBuilder();
            message.AppendLine($"{StringUtils.FirstCharToUpper(city)}. Погода на сегодня:");
            message.AppendLine($"Сейчас температура: {currentTemp}°");
            message.AppendLine($"Max: {maxTemp}°. Min: {minTemp}°");
            var windInfo = GetWindInfo(Math.Round(weatherRoot.Wind.Speed));
            message.AppendLine($"{windInfo}");
            message.AppendLine($"На улице: {weatherRoot.Weather[0].Description}");
            return message.ToString();
        }

        private string GetWindInfo(double windSpeed)
        {
            var windPower = GetWindPower(windSpeed);
            switch (windPower) {
                case WindPower.Quiet:
                    return $"Ветер еле заметен: {windSpeed} м/с";
                case WindPower.Easy:
                    return $"Слабый ветер: {windSpeed} м/с";
                case WindPower.Medium:
                    return $"Ветер умеренный: {windSpeed} м/с";
                case WindPower.Strong:
                    return $"Сильный ветер: {windSpeed} м/с";
                case WindPower.Powerful:
                    return $"Вас может сдуть. Ветер: {windSpeed} м/с";
                case WindPower.Hurricane:
                    return $"Ураган, лучше не выходите из дома. Ветер: {windSpeed} м/с";
                default:
                    return "";
            }
        }
        
        private WindPower GetWindPower(double windSpeed)
        {
            WindPower windPower = WindPower.None;

            if (windSpeed <= 1.5) {
                windPower = WindPower.Quiet;
            }
            else if (windSpeed > 1.6 && windSpeed <= 3.3) {
                windPower = WindPower.Easy;
            }
            else if (windSpeed > 3.4 && windSpeed <= 6.9) {
                windPower = WindPower.Medium;
            }
            else if (windSpeed > 7.0 && windSpeed <= 10.8) {
                windPower = WindPower.Strong;
            }
            else if (windSpeed > 10.9 && windSpeed <= 13.9) {
                windPower = WindPower.Powerful;
            }
            else if (windSpeed > 14.0) {
                windPower = WindPower.Hurricane;
            }

            return windPower;
        }

        private TempFeeling GetTemperatureFeeling(double temp)
        {
            TempFeeling feeling = TempFeeling.None;
            if (temp <= -11) {
                feeling = TempFeeling.Dubak;
            }
            else if (temp > -10.9 && temp <= -5.0) {
                feeling = TempFeeling.Cold;
            }
            else if (temp > -4.9 && temp <= 2.0) {
                feeling = TempFeeling.Freeze;
            }
            else if (temp > 2.1 && temp <= 12.0) {
                feeling = TempFeeling.Cool;
            }
            else if (temp > 12.1 && temp <= 16.9) {
                feeling = TempFeeling.PreComfort;
            }
            else if (temp > 17.0 && temp <= 26) {
                feeling = TempFeeling.Comfort;
            }
            else if (temp > 26.1 && temp <= 30) {
                feeling = TempFeeling.Hot;
            }
            else if (temp > 30) {
                feeling = TempFeeling.Roast;
            }

            return feeling;
        }
    }
}