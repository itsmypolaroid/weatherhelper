using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WeatherHelper.Services;
using WeatherHelper.Utils;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using User = WeatherHelper.Models.User;

namespace WeatherHelper
{
    public class TelegramBot
    {
        private readonly List<string> _listOfHello = new List<string>
        {
            "Hello", "Aloha!", "It's again me!", "Hola, amigo!",
            "Привет!", "О еще один, кожанный... Здравствуйте!", "Hello Comrade!",
            "Bonjour!", "Welcome back! Me..", "BORN TO KILL... oops, hi!"
        };

        private const string START_COMMAND = "/start";
        private const string HELP_COMMAND = "/help";
        private const string PART_CITY_COMMAND = "город ";
        private const string PART_TIMER_COMMAND = "время ";
        private const string STOP_TIMER_COMMAND = "стоп";
        private const string PART_WEATHER_COMMAND = "погода";
        private const string DEFAULT_RESPONSE = "Извините, я создан не для общения.";

        private const string TOKEN = "945482347:AAHgyiRQ-mRuVQV5n5_t5mLDds-6mAPkQPc";

        private readonly List<User> _users;
        private readonly ITelegramBotClient _botClient;
        private readonly IWeatherService _weatherService;
        private readonly Regex _timePattern = new Regex("^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$");

        public TelegramBot(IWebProxy proxy, IWeatherService weatherService)
        {
            _weatherService = weatherService;
            _users = new List<User>();

            _botClient = new TelegramBotClient(TOKEN, proxy) {Timeout = TimeSpan.FromSeconds(10)};
            _botClient.OnMessage += OnMessage;
            _botClient.OnMessageEdited += BotClientOnOnMessageEdited;
        }

        private void BotClientOnOnMessageEdited(object sender, MessageEventArgs e)
        {
            Console.WriteLine("editing");
        }

        private async void OnMessage(object sender, MessageEventArgs e)
        {
            try {
                var message = e.Message;
                if (message == null) return;
                var chatId = message.Chat.Id;

                if (message.Text.Equals(HELP_COMMAND)) {
                    var infoMessage = GetHelpMessage();
                    await _botClient.SendTextMessageAsync(chatId, infoMessage);
                    return;
                }

                if (message.Text.ToLower().Equals(START_COMMAND)) {
                    if (_users.All(user => user.Id != chatId)) {
                        _users.Add(new User() {Id = chatId});
                    }

                    var helloMessage = Chooser.GetRandomString(_listOfHello) + "\n";
                    var startMessage = helloMessage + GetInfoMessage();
                    await _botClient.SendTextMessageAsync(chatId, startMessage);
                    return;
                }

                if (message.Text.ToLower().TrimStart().StartsWith(PART_CITY_COMMAND)) {
                    AddUserSettings(message, PART_CITY_COMMAND);
                    await _botClient.SendTextMessageAsync(chatId, "Ваш город добавлен, спасибо!");
                    return;
                }

                if (message.Text.ToLower().TrimStart().StartsWith(PART_TIMER_COMMAND)) {
                    AddUserSettings(message, PART_TIMER_COMMAND);
                    await _botClient.SendTextMessageAsync(chatId, "Таймер установлен");
                    var user = _users.First(s => s.Id == chatId);
                    var time = DateTime.ParseExact(user.Timer, "H:mm", CultureInfo.InvariantCulture);
                    Scheduler.IntervalInDays(time.Hour, time.Minute, 1, chatId,
                        async () => await SendWeatherMessage(chatId));
                    return;
                }

                if (message.Text.ToLower().Trim().Equals(STOP_TIMER_COMMAND)) {
                    var user = _users.First(s => s.Id == chatId);
                    if (_users.Contains(user)) {
                        user.Timer = string.Empty;
                        Scheduler.RemoveScheduleTask(user.Id);
                    }

                    await _botClient.SendTextMessageAsync(chatId, "Таймер остановлен");
                    return;
                }

                if (message.Text.ToLower().StartsWith(PART_WEATHER_COMMAND)) {
                    await SendWeatherMessage(chatId);
                    return;
                }

                await _botClient.SendTextMessageAsync(chatId, DEFAULT_RESPONSE);
            }
            catch (Exception exception) {
                if (exception.Message.Contains("404 (Not Found)")) {
                    await _botClient.SendTextMessageAsync(e.Message.Chat.Id, "Неверно указан город");
                }
                else {
                    await _botClient.SendTextMessageAsync(e.Message.Chat.Id, exception.Message);
                }
            }
        }

        private async Task SendWeatherMessage(long chatId)
        {
            string city = "";
            foreach (var user in _users.Where(user => user.Id == chatId)) {
                city = user.City;
            }

            var weatherMessage = await _weatherService.GetWeatherAsync(city);
            await _botClient.SendTextMessageAsync(chatId, weatherMessage);
        }

        private void AddUserSettings(Message message, string partOfCommand)
        {
            var commandMessage = message.Text.ToLower().TrimStart();
            var value = commandMessage.Substring(partOfCommand.Length,
                commandMessage.Length - partOfCommand.Length);

            var user = _users.First(s => s.Id == message.Chat.Id);

            if (partOfCommand.Equals(PART_CITY_COMMAND)) {
                user.City = value;
            }
            else if (partOfCommand.Equals(PART_TIMER_COMMAND)) {
                if (_timePattern.IsMatch(value))
                    user.Timer = value;    
                else
                    throw new ArgumentException("Неверный формат времени");
            }
        }

        private string GetInfoMessage()
        {
            var startMessage = new StringBuilder();
            startMessage.AppendLine();
            startMessage.AppendLine("Я покажу Вам погоду в городе, который вы выберете.");
//            startMessage.AppendLine("А также могу уведомить Вас о погоде на день и помочь выбрать наряд.");
            startMessage.AppendLine("Пожалуйста, укажите город в следующем формате:");
            startMessage.AppendLine("город <Ваш город>.\nНапример: город Москва");
            startMessage.AppendLine("Если хотите узнать погоду, напишите \"погода\"");
            startMessage.AppendLine("Для уведомления в удобное Вам время воспользуйтесь форматом:");
            startMessage.AppendLine("время <HH:mm>.\nНапример: время 21:00");
            startMessage.AppendLine("Для остановки уведомлений, напишите \"стоп\"");
            return startMessage.ToString();
        }

//TODO 27 сентября 2019 г.: Придумать что-нибудь с этими огромными и схожими методами
        private string GetHelpMessage()
        {
            var startMessage = new StringBuilder();
            startMessage.AppendLine("Указать или поменять город командой:");
            startMessage.AppendLine("город <Ваш город>.\nНапример: город Москва");
            startMessage.AppendLine("Если хотите узнать погоду, напишите \"погода\"\n");
            startMessage.AppendLine("Команда для уведомлений о погоде в указанное время:");
            startMessage.AppendLine("время <HH:mm>.\nНапример: время 21:00");
            startMessage.AppendLine("Для остановки уведомлений, напишите \"стоп\"");
            return startMessage.ToString();
        }

        /// <summary>
        /// Запускает бота
        /// </summary>
        public void StartBot()
        {
            _botClient.StartReceiving(Array.Empty<UpdateType>());
        }
    }
}