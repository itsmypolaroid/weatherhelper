using System;
using WeatherHelper.Database;
using WeatherHelper.Repository;

namespace WeatherHelper
{
    public class UnitOfWork : IDisposable
    {
        private readonly WeatherContext _context;
        public IClothesRepository Clothes { get; private set; }
        public IUserRepository Users { get; private set; }

        public UnitOfWork(WeatherContext context)
        {
            _context = context;
            Clothes = new ClotheRepository(_context);
            Users = new UserRepository(_context);
        }
        
        public async void SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}