﻿using System;
using System.Net;
using MihaZupan;
using WeatherHelper.Services;

namespace WeatherHelper
{
    internal static class Program
    {
        private static IWeatherService _weatherService;

        static void Main(string[] args)
        {
            // for more proxies http://spys.one/proxys

            _weatherService = new WeatherService();

            var proxy = new HttpToSocks5Proxy("96.113.176.101", 1080);
            var weatherBot = new TelegramBot(proxy, _weatherService);
            weatherBot.StartBot();

            Console.ReadKey();
        }
    }
}