using System.Collections.Generic;
using System.Threading.Tasks;
using WeatherHelper.Models.ClothesModels;
using WeatherHelper.Models.WeatherModels;

namespace WeatherHelper
{
    public class ClothesHelper
    {
         private const string CLOTHES_BASE_URI = "";

        private ClothesRoot _clothesRoot;

        public ClothesHelper()
        {
            _clothesRoot = new ClothesRoot
            {
                OuterWear = new OuterWear {Jacket = new List<string> {"Кожанная куртка", "Белый пуховик"}},
                HeadWear = new HeadWear {Hats = new List<string> {"Белая шапка", "Черная шапка"}},
                TopWear = new TopWear {Sweater = new List<string> {"Сереневый свитер", "Белый джемпер"}},
                BottomWear =
                    new BottomWear {Jeans = new List<string> {"Синие джинсы с дырками", "Черные джинсы"}},
                UnderWear =
                    new UnderWear {Tights = new List<string> {"Черные плотные колготки", "Бежевые колготки"}},
                FootWear = new FootWear {Sneakers = new List<string> {"Белые кроссовки", "Черные кроссовки"}},
                Accessories = new List<Accessory>
                {
                    new Accessory
                    {
                        Rings = new List<string> {"Кольцо с аметистом", "Кольцо из белого золота"}
                    },
                    new Accessory
                    {
                        Earrings = new List<string> {"Сережки с аметистом", "Сережки с жемчугом с алишки"}
                    }
                }
            };
        }
        
        public async Task<string> GetClothesAsync(TempFeeling tempFeeling, WindPower windPower, string weather)
        {
            return await Task.Run(() => GetMostSuitableClothes(_clothesRoot));
        }

        private string GetMostSuitableClothes(ClothesRoot clothesRoot)
        {
            return "";
        }

    }
}